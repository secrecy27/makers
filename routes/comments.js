var express = require('express');
var router = express.Router();
var db = require('../utils/db');
var Comments = require('../libs/comments');
const jsonfile = require('jsonfile')
var file = 'makers_logs.json';


/* GET users listing. */
router.get('/', function(req, res, next) {
    res.send('respond with a resource');
});

router.post('/', function (req, res , next) {
    var body = req.body;
    console.log('body=>', body);

    db.connectDB()
        .then(() => {
            createComment(body)
                .then(result => {
                    console.log('result=>', result)
                    res.status(200).send(result);
                })
        })

    jsonfile.readFile(file)
        .then((obj) => {

            obj.push(body)
            console.log('obj=>', obj);

            jsonfile.writeFile(file, obj, { spaces: 2, EOL: '\r\n' }, function (err) {
                if (err) {
                    console.error(err)
                    // res.status(500).send({success:"fail"})
                }
                // res.status(200).send({success:"ok"})
            })
        })
        .catch(err => {
            console.log('catch err=>', err);

            let obj = []
            obj.push(body)

            jsonfile.writeFile(file, obj, { spaces: 2, EOL: '\r\n' }, function (err) {
                if (err) {
                    console.error(err)
                    // res.status(500).send({success:"fail"})
                }
                // res.status(200).send({success:"ok"})
            })
        })

})

function createComment (data) {
    return new Promise((resolve, reject) => {
        console.log(data)

        var comments = new Comments(data)
        comments.save(function (err, result) {
            if (err) {
                reject(err);
            } else {
                console.log('createComments done: ' + result)
                resolve(result)
            }
        })
    })
}
module.exports = router;
