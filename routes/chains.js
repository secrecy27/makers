var express = require('express');
var router = express.Router();
const jsonfile = require('jsonfile')
const path = require('path');

var fileName = '../blockFiles/chain.json';
const file = path.join(__dirname, fileName)
// var file = 'chain.json';

router.get('/', function (req, res, next) {
    jsonfile.readFile(file)
        .then((obj) => {
            console.log('obj=>', obj)
            res.status(200).send(obj);
        })
});

router.get('/:hashId', function (req, res, next) {
    var hashId = req.params.hashId;

    jsonfile.readFile(file)
        .then((obj) => {
            // console.log('obj=>', obj)

            var blocks = obj['Blocks']

            for (var i = 0; i < blocks.length; i ++) {

                if (hashId == blocks[i]['hash']) {
                    console.log('blocks=>', blocks[i])
                    res.status(200).send(blocks[i]);
                    break;
                } else {

                    if (i >= blocks.length-1) {
                        var error = "can't find hash"
                        res.status(500).send(error);
                        break;
                    }
                }


            }

        })
});

module.exports = router;
