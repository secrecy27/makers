var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;
var Comment = require('./comments')

var articlesSchema = new Schema({
    id: String,
    title: String,
    regDate: String,
    content: String,
    writer: String,
    likes: Number,
    comments:[Comment]
});

module.exports = articlesSchema;
